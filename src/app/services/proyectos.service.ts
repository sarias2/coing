import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Resolve } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as $ from "jquery";

// import {AngularFireDatabase} from 'angularfire2/database';

export interface IMessage {
  name?: string,
  email?: string,
  message?: string
}

@Injectable()
export class ProyectosService{

  private emailUrl = '/assets/email.php';

  proyectos:any[] = [];
  recientes:any[] = [];
  clientes:any[] = [];

  constructor(private http: Http) {
    // this.proyectos = af.list('coing-fc149');
    var config = {
      apiKey: "AIzaSyCL0OONE5sR34wVO4FzDySKKg5iAWK59z8",
      authDomain: "AIzaSyCL0OONE5sR34wVO4FzDySKKg5iAWK59z8",
      databaseURL: "https://coing-fc149.firebaseio.com",
      projectId: "coing-fc149",
      storageBucket: "coing-fc149.appspot.com",
      messagingSenderId: "728146593975"
    };
    firebase.initializeApp(config);


  }

  sendEmail(message: IMessage): Observable<IMessage> | any {
    return this.http.post(this.emailUrl, message)
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })
  }

  getProyectos(){
    var database = firebase.database();
    var ref = database.ref('/proyectos');
    var proyectos:any[] = [];
    var clientes:any[] = [];
    ref.on('value', function(data){
      let projects = data.val();
      for (let i = 0; i < projects.length; i++) {
        proyectos.push(projects[i]);
        clientes.push(projects[i].Cliente);
      }
    });
    this.proyectos = proyectos;
    return this.proyectos;
  }
  getRecientes(){
    var database = firebase.database();
    var ref = database.ref('/recientes');
    var recientes:any[] = []
    ref.on('value', function(data){
      let recent = data.val();
      for (let i = 0; i < recent.length; i++) {
        recientes.push(recent[i]);
      }
    });
    this.recientes = recientes;
      return this.recientes;
  }
  buscarProyectos(termino:string){
    let proyectosArr:Proyecto[] = [];
    termino = termino.toLowerCase();

    for(let proy of this.proyectos){
      let cliente = proy.Cliente.toLowerCase();
      let proyecto = proy.Proyecto.toLowerCase();
      if ((cliente.indexOf(termino) >= 0)||(proyecto.indexOf(termino)>=0)) {
        proyectosArr.push(proy);
      }
    }
    return proyectosArr;
  }

  agruparProyectos(){
    for (let variable of this.proyectos) {
      this.clientes.push(variable.Cliente);
    }
    let unique_array = []
    for(let i = 0;i < this.clientes.length; i++){
        if(unique_array.indexOf(this.clientes[i]) == -1){
            unique_array.push(this.clientes[i])
        }
    }
    this.clientes = unique_array;

    let grupo:any[] = [];
    for (let cliente of this.clientes) {
      let arr:any[] = [];
      for (let proyecto of this.proyectos) {
        if (proyecto.Cliente == cliente) {
          arr.push(proyecto);
        }
      }
      grupo.push(arr);
    }
    console.log(grupo);
    return(grupo);
  }

}


export interface Proyecto{
  Cliente:string;
  Proyecto:string;
  Periodo:string;
  Foto:string;
}

// IDEA: Falta CR Torres de Camelot, ACUAVIVA S.A. 
