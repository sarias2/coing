import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { TratamientoDeDatosComponent } from './components/tratamiento-de-datos/tratamiento-de-datos.component';

const APP_ROUTES: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'empresa', component: EmpresaComponent },
  { path: 'servicios', component: ServiciosComponent },
  { path: 'buscar/:termino', component: BuscadorComponent },
  { path: 'tratamiento-de-datos', component: TratamientoDeDatosComponent },
  { path: 'buscar', pathMatch: 'full', redirectTo: 'buscar/.' },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:false});
