import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';

//Rutas
import { APP_ROUTING } from './app.routes';

//Servicios
import { ProyectosService } from './services/proyectos.service';
import { HttpModule } from '@angular/http';
//Componentes
import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { EjecucionComponent } from './components/ejecucion/ejecucion.component';
import { ExperienciaComponent } from './components/experiencia/experiencia.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { NavbarwhiteComponent } from './components/shared/navbarwhite/navbarwhite.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { TratamientoDeDatosComponent } from './components/tratamiento-de-datos/tratamiento-de-datos.component';
import { TrabajaComponent } from './components/trabaja-con-nosotros/trabaja-con-nosotros.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCL0OONE5sR34wVO4FzDySKKg5iAWK59z8",
  authDomain: "AIzaSyCL0OONE5sR34wVO4FzDySKKg5iAWK59z8",
  databaseURL: "https://coing-fc149.firebaseio.com",
  storageBucket: "coing-fc149.appspot.com",
  messagingSenderId: "728146593975"
};

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    EjecucionComponent,
    ExperienciaComponent,
    ContactoComponent,
    FooterComponent,
    NavbarComponent,
    NavbarwhiteComponent,
    EmpresaComponent,
    ServiciosComponent,
    BuscadorComponent,
    TratamientoDeDatosComponent,
    TrabajaComponent,
    ClientesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ProyectosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
