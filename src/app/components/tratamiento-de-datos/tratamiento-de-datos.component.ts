import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-tratamiento-de-datos',
  templateUrl: './tratamiento-de-datos.component.html',
  styles: []
})
export class TratamientoDeDatosComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    window.scrollTo(0, 0);
    $(".collapse-navbar").removeClass("show");
  }

}
