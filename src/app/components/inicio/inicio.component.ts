import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
// import interact = require('interactjs')

@Component({
  selector: 'app-principal',
  templateUrl: './inicio.component.html',
  styles: []
})
export class InicioComponent implements OnInit {


  constructor() {
  }

  ngOnInit() {

    var draggable = $('#logoPrincipal'); //element

    draggable.on('mousedown', function(e){
        var dr = $(this).addClass("drag").css("cursor","move");
        var height = dr.outerHeight();
        var width = dr.outerWidth();
        var ypos = 150,
        xpos = dr.offset().left + width - e.pageX;
        $(document.body).on('mousemove', function(e){
            var itop = 130;
            var ileft = e.pageX + xpos - width;
            if(dr.hasClass("drag")){
                dr.offset({top: itop,left: ileft});
                var $posFinal = e.pageX;
                var $corte = "rect("+"0,"+$posFinal+"px"+",100vh,"+"0)"
                $(".gris").css("clip", $corte);
            }
        }).on('mouseup', function(e){
                dr.removeClass("drag");
        });
    });
    // $(document).ready(function(){
    //   var $ancho = $("#logoPrincipal").width();
    //   if($("#logoPrincipal").mousepressed() == true){
    //     $(document).mousemove(function(e) {
    //       $('#logoPrincipal').offset({
    //         left: e.pageX - $ancho/2,
    //         top: 150
    //       });
    //       var $posFinal = e.pageX;
    //       var $corte = "rect("+"0,"+$posFinal+"px"+",100vh,"+"0)"
    //       $(".gris").css("clip", $corte);
    //     });
    //   }
    // });

  }
}
