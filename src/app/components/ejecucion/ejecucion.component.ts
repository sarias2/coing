import { Component, OnInit } from '@angular/core';
import { ProyectosService } from '../../services/proyectos.service';

@Component({
  selector: 'app-ejecucion',
  templateUrl: './ejecucion.component.html',
  styles: []
})
export class EjecucionComponent implements OnInit {

  recientes:any[] = [];

  constructor(private recientesService:ProyectosService) { }

  ngOnInit() {
    this.recientes = this.recientesService.getRecientes();
  }

}
