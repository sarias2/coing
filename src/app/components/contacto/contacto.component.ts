import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ProyectosService, IMessage } from '../../services/proyectos.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styles: []
})
export class ContactoComponent implements OnInit {

  title = 'Contacto COING';
  message: IMessage = {};


  constructor(private proyectosService:ProyectosService, private router: Router) {

  }

  sendEmail(message: IMessage) {
    this.proyectosService.sendEmail(message).subscribe(res => {
      console.log('AppComponent Success', res);
    }, error => {
      // console.log('AppComponent Error', error);
    })
  }
  ngOnInit() {
    $(document).ready(function name(parameter) {
      $(".btn-coing").click(function(){
        alert("Tu mensaje ha sido enviado");
        this.router.navigate(['/inicio']);
      });
    });
  }
}
