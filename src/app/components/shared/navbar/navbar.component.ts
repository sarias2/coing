import { Component, OnInit } from '@angular/core';
import { RouterLinkActive } from '@angular/router';
import * as $ from "jquery";
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var $open = true;
    $(".nav-link").mouseover(function(){
      $(this).toggleClass("animated bounce");
    });
    $(".navbar-toggler").click(function(){
      if ($("#carouselExampleSlidesOnly").css("filter") == "blur(5px)") {
        $("#carouselExampleSlidesOnly").css("filter","blur(0)");
      }else{
        $("#carouselExampleSlidesOnly").css("filter","blur(5px)");
      }
    });
  }
}
