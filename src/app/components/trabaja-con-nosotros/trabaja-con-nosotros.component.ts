import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'trabaja-con-nosotros',
  templateUrl: 'trabaja-con-nosotros.component.html',
})
export class TrabajaComponent implements OnInit {
  constructor() {  }

  ngOnInit() {
    $("h3").click(function(){
      $(".contenedor-trabaja").addClass("tn-cerrado")
    });
    $("h2").click(function(){
      $(".contenedor-trabaja").removeClass("tn-cerrado")
    });
  }
}
