import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styles: []
})
export class EmpresaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).mousemove(function(e) {
      $('.hexa').offset({
        left: e.pageX - 200,
        top: e.pageY - 200
      });
    });
  }
}
