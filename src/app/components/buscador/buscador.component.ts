import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProyectosService } from '../../services/proyectos.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import * as firebase from 'firebase';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styles: []
})
export class BuscadorComponent implements OnInit {

  proyectos:any[] = [];
  clientes:any[] = [];
  constructor( private activatedRoute:ActivatedRoute, private proyectosService:ProyectosService, private router:Router ) {

  }
  ngOnInit() {
    $(document).mousemove(function(e) {
      $('.hexa').offset({
        left: e.pageX,
        top: e.pageY
      });
    });

    this.activatedRoute.params.subscribe(params =>{
      this.proyectos = this.proyectosService.buscarProyectos(params['termino']);
    });

    this.proyectos = this.proyectosService.getProyectos();
    this.clientes = this.proyectosService.agruparProyectos();


  }
  buscarProyecto(termino:string){
    this.router.navigate(['/buscar',termino]);
  }
}
