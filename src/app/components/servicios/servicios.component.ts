import { Component, OnInit } from '@angular/core';
import * as $ from "jquery/dist/jquery.min.js";
@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styles: []
})
export class ServiciosComponent implements OnInit {
  constructor() {

  }

  ngOnInit() {
    $(document).ready(function name(parameter) {
      $(document).mousemove(function(e) {
        $('.hexa').offset({
          left: e.pageX - 200,
          top: e.pageY - 200
        });
      });
      $(".cc").children().click(function(){
        $(this).toggleClass("fondo-negro");
        $(this).toggleClass("cerrado");
        $(this).find($(".fa")).toggleClass("cerrado");
        $(this).find($(".texto-servicios")).toggleClass("fadeIn");
        $(this).find($(".texto-servicios")).toggleClass("fadeOut");
      });
    });
  }
}
